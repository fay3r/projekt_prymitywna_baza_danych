/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spis;

import java.io.*;
import java.util.Date;
import java.util.LinkedList;
import java.util.ListIterator;

/**
 *
 *
 */
public class ListaPowiazana extends LinkedList implements Serializable, Comparable {                 // rozszerza klase bo potrzebuje zmienic pare metod

    private Date kiedy;
    private static ListIterator<Dziecko> iter;

    @Override
    public void add(int index, Object element) {
        kiedy = new Date();
        super.add(index, element); //To change body of generated methods, choose Tools | Templates.
    }

    public String ostatniaModyfikacja() {
        return kiedy.toLocaleString();
    }

    public void setKiedy(Date kiedy) {
        this.kiedy = kiedy;
    }

    public static String wypisz(LinkedList A) {

        iter = A.listIterator();
        if (!iter.hasNext()) {
            return null;
        }
        StringBuilder tekst = new StringBuilder();                  //buduje prawie stringa i pozniej toString
        for (Object A1 : A) {
            tekst.append(iter.next().toString()).append("\n");      // append dopisuje do striga to co w nawiasie
        }
        return tekst.toString();
    }

    @Override
    public int compareTo(Object arg0) {
        throw new UnsupportedOperationException("Not supported yet.");  //  ogolna compareTo.
    }

    public static boolean sprawdz(String s) {                                    //sprawdza czy nie ma samych spacji
        boolean x = true;
        final char spacja = ' ';
        char[] dosprawdzenia = s.toCharArray();
        if (dosprawdzenia.length == 0) {
            return true;
        }
        for (int i = 0; i < dosprawdzenia.length; i++) {
            x = true;
            if (Character.compare(spacja, dosprawdzenia[i]) != 0) {
                x = false;
            }
            return x;
        }

        return x;
    }

    public static void zapiszJakoTxt(ListaPowiazana a, String nazwa) throws IOException {       //zapisuje LinkedArray
        File plik = new File(nazwa + "_druk.txt");
        try (PrintWriter writer = new PrintWriter(new FileWriter(plik))) {
            iter = a.listIterator();
            writer.println("Ilosc pozycji: " + a.size());
            
            while (iter.hasNext() == true) {
                
                writer.println(iter.next().toString());
            }
        }

    }
//        *********************************************** wczyt z txt (niepotrzebne)************************************************
//    public static ListaPowiazana wczytajZPliku(BufferedReader inS) throws Wyjatek, IOException {     // wczytywanie z pliku Listy z obiektami typu dziecko zamiast tylko stringow
//        ListaPowiazana nowa = new ListaPowiazana();
//        int x = Integer.parseInt(inS.readLine());
//        for (int i = 0; i < x; i++) {
//            String linia = inS.readLine();
//            StringTokenizer tokeny = new StringTokenizer(linia.substring(Integer.toString(i + 1).length() + 3), "\t");     // token dzieli mi stringa na czesci od \t do \t
//            String nazwisko = tokeny.nextToken();
//            String imie = tokeny.nextToken();
//            String data = tokeny.nextToken();
//            DataUrodzenia dataU;
//            dataU = DataUrodzenia.zamien(data);
//            String imieO = tokeny.nextToken();
//            String adres = tokeny.nextToken();
//            int numer = Integer.parseInt(tokeny.nextToken());
//            String skl = tokeny.nextToken();
//            Boolean skladka;
//            if (skl.equals("tak")) {
//                skladka = true;
//            } else {
//                skladka = false;
//            }
//
//            nowa.add(new Dziecko(i + 1, nazwisko, imie, dataU, new Opiekun(imieO, adres, numer, skladka)));
//        }
//        return nowa;
//    }

    public static ListaPowiazana wyszukiwanie(ListaPowiazana oryginal, int indeks, String imie, String nazwisko) {      //wyszukiwanie wszystko w jednym
        if (indeks != 0) {
            oryginal = wyszukajIndeks(oryginal, indeks);
        }
        oryginal = wyszukajImie(oryginal, imie);
        oryginal = wyszukajNazwisko(oryginal, nazwisko);

        return oryginal;
    }

    private static ListaPowiazana wyszukajImie(ListaPowiazana oryginal, String imie) {
        if (sprawdz(imie) == true) {

            return oryginal;
        }
        ListaPowiazana znalezione = new ListaPowiazana();
        Dziecko checker;
        iter = oryginal.listIterator();
        for (Object oryginal1 : oryginal) {
            checker = iter.next();

            if (checker.getImie().compareTo(imie) == 0) {

                znalezione.addLast(checker);
            }
        }
        return znalezione;
    }

    private static ListaPowiazana wyszukajNazwisko(ListaPowiazana oryginal, String nazwisko) {
        if (sprawdz(nazwisko) == true) {

            return oryginal;

        }
        ListaPowiazana znalezione = new ListaPowiazana();
        Dziecko checker;
        iter = oryginal.listIterator();
        for (Object oryginal1 : oryginal) {
            checker = iter.next();

            if (checker.getNazwisko().compareTo(nazwisko) == 0) {

                znalezione.addLast(checker);
            }
        }
        return znalezione;
    }

    private static ListaPowiazana wyszukajIndeks(ListaPowiazana oryginal, int indeks) {
        if (indeks < 0) {
            return null;
        }

        ListaPowiazana znalezione = new ListaPowiazana();
        Dziecko checker;
        iter = oryginal.listIterator();
        for (Object oryginal1 : oryginal) {
            checker = iter.next();
            if (checker.getNumer() == indeks) {

                znalezione.addLast(checker);
            }
        }

        return znalezione;
    }

    public static ListaPowiazana usunElement(ListaPowiazana oryginal, int indeks) {
        ListaPowiazana nowa = new ListaPowiazana();

        oryginal.remove(indeks - 1);
        iter = oryginal.listIterator();
        for (int i = 0; i < oryginal.size(); i++) {
            Dziecko x = iter.next();
            x.setNumer(i + 1);
            nowa.add(x);
        }

        return nowa;
    }

}
