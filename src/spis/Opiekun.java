/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spis;

import java.io.*;

/**
 *
 * @author Patryk
 */
public class Opiekun implements Serializable {

    private String imie;
    private String adres;
    private int numer;
    private String składka;

    public Opiekun(String imie, String adres, int numer, boolean skladka) {
        this.imie = imie;
        if(ListaPowiazana.sprawdz(imie)==true){this.imie="brak";}
        this.adres = adres;
        if(ListaPowiazana.sprawdz(adres)==true){this.adres="brak";}
        this.numer = numer;
        if (skladka == true)
         {
            this.składka = "tak";
        }else{
            this.składka = "nie";
        }
    }

    @Override
    public String toString() {
        return imie +"\t"+ adres +"\t"+ numer +"\t"+ składka;
    }

    /**
     * @return the imie
     */
    public String getImie() {
        return imie;
    }

    /**
     * @param imie the imie to set
     */
    public void setImie(String imie) {
        this.imie = imie;
    }

    /**
     * @return the adres
     */
    public String getAdres() {
        return adres;
    }

    /**
     * @param adres the adres to set
     */
    public void setAdres(String adres) {
        this.adres = adres;
    }

    /**
     * @return the numer
     */
    public int getNumer() {
        return numer;
    }

    /**
     * @param numer the numer to set
     */
    public void setNumer(int numer) {
        this.numer = numer;
    }

    /**
     * @return the składka
     */
    public String getSkładka() {
        return składka;
    }

    /**
     * @param składka the składka to set
     */
    public void setSkładka(String składka) {
        this.składka = składka;
    }

}
