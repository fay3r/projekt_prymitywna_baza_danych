/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spis;

import dataUrodzenia.*;
import java.io.*;

/**
 *
 * @author Patryk
 */
public class Dziecko implements Serializable{
    
    private String nazwisko,imie;
    private DataUrodzenia data;
    private Opiekun rodzic;
    private int numer;
    
   
    public Dziecko(int numer, String nazwisko, String imie, DataUrodzenia data, Opiekun rodzic) {
        this.nazwisko = nazwisko;      
        this.imie = imie;
        this.data = data;
        this.rodzic = rodzic;
        this.numer=numer;
       
    }

    @Override
    public String toString() {
        return numer +".  "+ nazwisko  +"\t"+ imie +"\t"+ data  +"\t"+ rodzic ;
    }
    /**
     * @return the nazwisko
     */
    public String getNazwisko() {
        return nazwisko;
    }

    /**
     * @param nazwisko the nazwisko to set
     */
    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    /**
     * @return the imie
     */
    public String getImie() {
        return imie;
    }

    /**
     * @param imie the imie to set
     */
    public void setImie(String imie) {
        this.imie = imie;
    }

    /**
     * @return the data
     */
    public DataUrodzenia getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(DataUrodzenia data) {
        this.data = data;
    }

    /**
     * @return the rodzic
     */
    public Opiekun getRodzic() {
        return rodzic;
    }

    /**
     * @param rodzic the rodzic to set
     */
    public void setRodzic(Opiekun rodzic) {
        this.rodzic = rodzic;
    }

    public int getNumer() {
        return numer;
    }

    public void setNumer(int numer) {
        this.numer = numer;
    }
      
}


