package dataUrodzenia;

import java.io.Serializable;
import java.util.StringTokenizer;

public class DataUrodzenia implements Serializable{

    private int dzien;
    private int rok;
    private Miesiac miesiac;

    public DataUrodzenia(int dzien, int miesiac, int rok) throws Wyjatek {
        this.dzien = dzien;
        this.rok = rok;
        this.miesiac = Miesiace.getMiesiac(miesiac);
        sprawdzDni();
    }

    private void sprawdzDni() throws Wyjatek {
        int a = getMiesiac().getLiczbadni();
        int b = getDzien();
        int c = this.rok % 4;

        if (c == 0 && a == 28) {
            a = 29;
        }
        if (b < 1 || b > a) {
            throw new Wyjatek("  zly numer dnia  ");

        }
    }

    public static int czyPrzestepny(int rok) {

        if ((rok % 4 == 0 && rok % 100 != 0) || rok % 400 == 0) {
            return 29;
        }

        return 28;
    }



    @Override
    public String toString() {
            return  dzien + "." + miesiac.getNumer() + "." + rok;
        }
    
    public static DataUrodzenia zamien( String s) throws Wyjatek{                   //zamienia stringa na DateUrodzenia 
        DataUrodzenia data;
        StringTokenizer tokeny = new StringTokenizer(s,".");
        int d,m,r;
        d=Integer.parseInt(tokeny.nextToken());
        m=Integer.parseInt(tokeny.nextToken());
        r=Integer.parseInt(tokeny.nextToken());
        data=new DataUrodzenia(d, m, r);
        
        return data;
    }

    

    public int getDzien() {
        return dzien;
    }

    public void setDzien(int dzien) {
        this.dzien = dzien;
    }

    public int getRok() {
        return rok;
    }

    public void setRok(int rok) {
        this.rok = rok;
    }

    public Miesiac getMiesiac() {
        return miesiac;
    }

    public void setMiesiac(Miesiac miesiac) {
        this.miesiac = miesiac;
    }

}
