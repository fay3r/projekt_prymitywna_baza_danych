/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataUrodzenia;

import java.io.Serializable;

/**
 *
 * @author Student
 */
public class Miesiac implements Serializable {

    private int numer;
    private int liczbadni;

    public Miesiac(int numer,int liczbadni) {
        this.numer = numer;
        this.liczbadni = liczbadni;
    }

    /**
     * @return the numer
     */
    public int getNumer() {
        return numer;
    }
    /**
     * @return the liczbadni
     */
    public int getLiczbadni() {
        return liczbadni;
    }

}
