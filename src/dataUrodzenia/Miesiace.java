/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataUrodzenia;

import java.io.Serializable;

/**
 *
 * @author Student
 */
public class Miesiace implements Serializable{
    static final Miesiac[] tab ={
        new Miesiac(1,31),
        new Miesiac(2,28),
        new Miesiac(3,31),
        new Miesiac(4,30),
        new Miesiac(5,31),
        new Miesiac(6,30),
        new Miesiac(7,31),
        new Miesiac(8,31),
        new Miesiac(9,30),
        new Miesiac(10,31),
        new Miesiac(11,30),
        new Miesiac(12,31)};

    public static Miesiac getMiesiac(int a) throws Wyjatek{
        if(a>12 || a<1) {
            throw new Wyjatek("Bledny miesiac");
        }
        return tab [a-1];
    }
    
            

    
}
