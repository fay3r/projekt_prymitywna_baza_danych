/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataUrodzenia;

/**
 *
 * @author Student
 */
public class Wyjatek extends Exception {

    /**
     * Creates a new instance of <code>Wyjatek</code> without detail message.
     */
    public Wyjatek() {
    }

    /**
     * Constructs an instance of <code>Wyjatek</code> with the specified detail
     * message.
     *
     * @param msg the detail message.
     */
    public Wyjatek(String msg) {
        super(msg);
    }
}
